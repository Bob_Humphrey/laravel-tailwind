<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AllController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PatternController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'show'])->middleware('page-cache')->name('home');

Route::get('/blade-ui-kit', function () {
  return view('blade-ui-kit.all');
})->middleware('page-cache');

Route::get('/blade-ui-kit/{component}', function ($component) {
  return view('blade-ui-kit.' . $component);
})->middleware('page-cache');

Route::get('/components', function () {
  return view('component-examples.all');
});

Route::get('/components/{component}', function ($component) {
  return view('component-examples.' . $component);
});

Route::get('/form-examples/{example}', function ($example) {
  return view('form-examples.' . $example);
})->middleware('page-cache');

Route::get('/misc/{pattern}', function ($pattern) {
  return view('misc.' . $pattern);
})->middleware('page-cache');

Route::get('/all', [AllController::class, 'show'])->middleware('page-cache');
Route::get('/{pattern}', [PatternController::class, 'show'])->middleware('page-cache');
