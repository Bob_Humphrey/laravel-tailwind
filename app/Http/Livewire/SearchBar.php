<?php

namespace App\Http\Livewire;

use App\Models\State;
use Livewire\Component;

class SearchBar extends Component
{
  public $query;
  public $states;
  public $highlightIndex;

  public function mount()
  {
    $this->resetState();
  }

  public function resetState()
  {
    $this->query = '';
    $this->states = [];
    $this->highlightIndex = 0;
  }

  public function incrementHighlight()
  {
    if ($this->highlightIndex === count($this->contacts) - 1) {
      $this->highlightIndex = 0;
      return;
    }
    $this->highlightIndex++;
  }

  public function decrementHighlight()
  {
    if ($this->highlightIndex === 0) {
      $this->highlightIndex = count($this->contacts) - 1;
      return;
    }
    $this->highlightIndex--;
  }

  public function selectState()
  {
    $state = $this->states[$this->highlightIndex] ?? null;
    if ($state) {
      $this->redirect(route('home', $state['id']));
    }
  }

  public function updatedQuery()
  {
    $this->states = State::where('name', 'like', '%' . $this->query . '%')
      ->get()
      ->toArray();
  }
  public function render()
  {
    return view('livewire.search-bar');
  }
}
