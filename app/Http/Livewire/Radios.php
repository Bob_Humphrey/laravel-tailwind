<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Radios extends Component
{
  public $size;

  public function mount()
  {
    $this->size = 'Small';
  }

  public function render()
  {
    return view('livewire.radios');
  }
}
