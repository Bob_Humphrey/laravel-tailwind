<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CheckboxSwitch extends Component
{
  public $published;

  public function mount()
  {
    $this->published = 0;
  }

  public function render()
  {
    return view('livewire.checkbox-switch');
  }
}
