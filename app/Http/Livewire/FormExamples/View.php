<?php

namespace App\Http\Livewire\FormExamples;

use Livewire\Component;

class View extends Component
{
  public function render()
  {
    return view('livewire.form-examples.view');
  }
}
