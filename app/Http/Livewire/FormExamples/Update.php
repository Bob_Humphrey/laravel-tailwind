<?php

namespace App\Http\Livewire\FormExamples;

use Livewire\Component;

class Update extends Component
{
  public function render()
  {
    return view('livewire.form-examples.update');
  }
}
