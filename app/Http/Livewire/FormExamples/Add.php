<?php

namespace App\Http\Livewire\FormExamples;

use Livewire\Component;

class Add extends Component
{
    public function render()
    {
        return view('livewire.form-examples.add');
    }
}
