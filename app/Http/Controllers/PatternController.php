<?php

namespace App\Http\Controllers;

use App\Actions\GetPatternsAction;
use Illuminate\Support\Facades\Storage;

class PatternController extends Controller
{
  public function show($pattern)
  {
    $patternName = $this->getPatternName($pattern);
    $file = $pattern . '.blade.php';
    $code = Storage::disk('patterns')->get($file);

    return view('pattern', [
      'patternName' => $patternName,
      'code' => $code,
      'key' => $pattern
    ]);
  }

  private function getPatternName($key)
  {
    $patterns = GetPatternsAction::execute();
    return $patterns[$key];
  }
}
