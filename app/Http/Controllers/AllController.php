<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actions\GetPatternsAction;
use Illuminate\Support\Facades\Storage;

class AllController extends Controller
{
  public function show()
  {
    $patternNames = GetPatternsAction::execute();
    $patterns = [];

    foreach ($patternNames as $key => $value) {
      $file = $key . '.blade.php';
      $code = Storage::disk('patterns')->get($file);
      $item = [
        'patternName' => $value,
        'code' => $code,
      ];
      $patterns[$key] = $item;
    }

    return view('all', [
      'patterns' => $patterns
    ]);
  }
}
