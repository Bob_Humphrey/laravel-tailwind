<?php

namespace App\Http\Controllers;

use App\Actions\GetPatternsAction;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
  public function show()
  {
    //
    //
    //
    $patterns = GetPatternsAction::execute();
    $collection = collect($patterns);
    $chunks = $collection->chunk(ceil($collection->count() / 3));

    return view('home', [
      'chunks' => $chunks,
    ]);
  }
}
