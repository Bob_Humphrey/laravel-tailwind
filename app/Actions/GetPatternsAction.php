<?php

namespace App\Actions;

class GetPatternsAction
{
  public static function execute()
  {

    // MUST BE IN ALPHA ORDER

    $patterns = [
      'action-buttons' => 'Action Buttons with Tooltips',
      'aligned-text-input' => 'Aligned Text Input',
      'buttons-with-rings' => 'Buttons with Rings',
      'cards-with-equal-height' => 'Cards with Equal Height',
      'cards-with-equal-height-and-button' => 'Cards with Equal Height and Button',
      'centered-box-with-centered-text' => 'Centered Box with Centered Text',
      'centered-button' => 'Centered Button',
      'centered-button-group' => 'Centered Button Group',
      'centered-column' => 'Centered Column',
      'centered-columns' => 'Centered Columns',
      'centered-horizontal-list' => 'Centered Horizontal List',
      'equal-height-column-grid' => 'Equal Height Column Grid',
      'flex-wrap' => 'Flex Wrap',
      'gradient-headline' => 'Gradient Headline',
      'image-with-text-overlay' => 'Image with Text Overlay',
      'list-with-dividers' => "List with Dividers",
      'nav-bar' => 'Nav Bar',
      'previous-next-buttons' => 'Previous/Next Buttons',
      'score' => 'Score',
      'simple-heading' => 'Simple Heading',
      'table' => 'Table',
      'table-filter-buttons' => 'Table Filter Buttons',
      'two-columns-with-rounded-corners' => 'Two Columns with Rounded Corners',
      'vertically-aligned-boxes' => 'Vertically Aligned Boxes',
    ];

    return $patterns;
  }
}
