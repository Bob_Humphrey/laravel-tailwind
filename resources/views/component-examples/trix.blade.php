<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Trix Editor
    </h2>

    @php
    $formCode = '
    {{-- BODY --}}

    <div class="pb-3">
      <div class="text-gray-700 text-sm uppercase">
        <x-label for="body" />
      </div>

      <x-body wire:model.lazy="body" id="body" :initial-value="$body" />

      <div class="">
        <x-error field="body" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>
    ';

    $componentCode = '
    @props([\'initialValue\' => \'\'])

    <div wire:ignore {{ $attributes }} x-data @trix-blur="$dispatch(\'change\', $event.target.value)">

      <input id="x" value="{{ $initialValue }}" type="hidden">
      <trix-editor input="x"></trix-editor>
    </div>
    ';
    @endphp

    @php
    $body = '';
    @endphp

    <div class="bg-gray-200 p-10 mb-10">
      <x-body-trix wire:model.lazy="body" id="body" class="bg-white p-6" :initial-value="$body" />
    </div>


    <h4 class="text-xl font-nunito_bold text-blue-900 mb-6">form.blade.php</h4>

    <pre><code class="php">{{ $formCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">components/body-trix.blade.php</h4>

    <pre><code class="html">{{ $componentCode }}</code></pre>

  </div>

</x-layouts.app>
