<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Radio Buttons
    </h2>

  @php
    $phpCode = '
      <?php

      namespace App\Http\Livewire;

      use Livewire\Component;

      class Radios extends Component
      {
        public $size;

        public function mount()
        {
          $this->size = \'Small\';
        }

        public function render()
        {
          return view(\'livewire.radios\');
        }
      }
    ';

    $bladeCode = '
      <div>
        <div class="text-xl mb-4">
          Size: {{ $size }}
        </div>

        <div class="flex space-x-6">
          <div>
            Select the size:
          </div>
          <div>
            <span>Small</span>
            <x-input type="radio" name="size" value="Small" wire:model="size" />
          </div>
          <div>
            <span>Medium</span>
            <x-input type="radio" name="size" value="Medium" wire:model="size" />
          </div>
          <div>
            <span>Large</span>
            <x-input type="radio" name="size" value="Large" wire:model="size" />
          </div>
        </div>
      </div>
    ';
    @endphp


    <div class="bg-gray-200 p-10 mb-10">
      @livewire('radios')
    </div>

    <h4 class="text-xl font-nunito_bold text-blue-900 mb-6">app/Http/Livewire/Radios.php</h4>

    <pre><code class="php">{{ $phpCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">radios.blade.php</h4>

    <pre><code class="html">{{ $bladeCode }}</code></pre>

  </div>

</x-layouts.app>
