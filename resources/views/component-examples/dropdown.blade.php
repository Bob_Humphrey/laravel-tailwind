<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Dropdown Menu
    </h2>

    @php
    $cssCode = '
    @tailwind base;

    [x-cloak] {
        display: none;
    }
    ';
    $phpCode = '
    <?php namespace App\View\Components;

    use Illuminate\View\Component;

    class MiscDropDown extends Component
    {
    /**
    * Create a new component instance.
    *
    * @return void
    */
    public function __construct()
    {
    //
    }

    /**
    * Get the view / contents that represent the component.
    *
    * @return \Illuminate\Contracts\View\View|string
    */
    public function render()
    {
    return view(\'components.misc-drop-down\');
    }
    }
    ';

    $bladeCode = '
    *php
    use Illuminate\Support\Str;
    $path = request()->path();
    $active = Str::of($path)->exactly(\'absolute-positioning\') || Str::of($path)->exactly(\'object-fit\') ||
    Str::of($path)->exactly(\'object-position\') ? true : false;
    *endphp

    <div x-data="{ open: false }" class="relative">
      <div class="{{ $active ? \'text-blue-500\' : \'text-blue-900\' }} hover:text-blue-500 py-2" @click="open = true">
        Miscellaneous
      </div>
      <div x-show="open" @click.away="open = false" x-cloak
        class="absolute z-10 w-full bg-gray-50 text-base text-blue-800  border-t border-gray-100 rounded-t-none shadow-md">
        <div class="py-2 px-4 hover:text-blue-500">
          <a href="{{ url(\'/absolute-positioning\') }}">Absolute Positioning</a>
        </div>
        <div class="py-2 px-4 hover:text-blue-500">
          <a href="{{ url(\'/object-fit\') }}">Object Fit</a>
        </div>
        <div class="py-2 px-4 hover:text-blue-500">
          <a href="{{ url(\'/object-position\') }}">Object Position</a>
        </div>
      </div>
    </div>
    ';
    @endphp

    <div class="mb-6">
      <x-misc-drop-down />
    </div>

    <h4 class="text-xl font-nunito_bold text-blue-900 mb-6">style.css to hide dropdown before javascript starts running</h4>

    <pre><code class="php">{{ $cssCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">app/View/Components</h4>

    <pre><code class="php">{{ $phpCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">misc-drop-down.blade.php</h4>

    <pre><code class="php">{{ $bladeCode }}</code></pre>

  </div>

</x-layouts.app>';
