<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Search Bar
    </h2>

  @php
    $phpCode = '
    <?php

      namespace App\Http\Livewire;

      use App\Models\State;
      use Livewire\Component;

      class SearchBar extends Component
      {
        public $query;
        public $states;
        public $highlightIndex;

        public function mount()
        {
          $this->resetState();
        }

        public function resetState()
        {
          $this->query = \'\';
          $this->states = [];
          $this->highlightIndex = 0;
        }

        public function incrementHighlight()
        {
          if ($this->highlightIndex === count($this->contacts) - 1) {
            $this->highlightIndex = 0;
            return;
          }
          $this->highlightIndex++;
        }

        public function decrementHighlight()
        {
          if ($this->highlightIndex === 0) {
            $this->highlightIndex = count($this->contacts) - 1;
            return;
          }
          $this->highlightIndex--;
        }

        public function selectState()
        {
          $state = $this->states[$this->highlightIndex] ?? null;
          if ($state) {
            $this->redirect(route(\'home\', $state[\'id\']));
          }
        }

        public function updatedQuery()
        {
          $this->states = State::where(\'name\', \'like\', \'%\' . $this->query . \'%\')
            ->get()
            ->toArray();
        }
        public function render()
        {
          return view(\'livewire.search-bar\');
        }
      }
    ';

    $bladeCode = '
    <div class="relative font-nunito_regular">
        <input type="text" class="py-1 px-3 rounded" placeholder="Search for a State..." wire:model="query" />

        @if (!empty($query))
          <div class="fixed top-0 right-0 bottom-0 left-0" wire:click="resetState">
          </div>

          <div
            class="absolute z-10 w-full bg-white text-base text-gray-900 border-t border-gray-100 rounded-t-none shadow-md">
            @if (!empty($states))
              @foreach ($states as $i => $state)
                <div class="py-2 px-4 hover:bg-gray-200">
                  <a href="{{ route(\'home\', $state[\'id\']) }}"
                    class="list-item {{ $highlightIndex === $i ? \'highlight\' : \'\' }}">
                    {{ $state[\'name\'] }}
                  </a>
                </div>
              @endforeach
            @else
              <div class="list-item">No results!</div>
            @endif
          </div>
        @endif
      </div>
    ';
    @endphp


    <div class="bg-gray-200 p-10 mb-10">
      @livewire('search-bar')
    </div>

    <h4 class="text-xl font-nunito_bold text-blue-900 mb-6">app/Http/Livewire/SearchBar.php</h4>

    <pre><code class="php">{{ $phpCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">search-bar.blade.php</h4>

    <pre><code class="html">{{ $bladeCode }}</code></pre>

  </div>

</x-layouts.app>
