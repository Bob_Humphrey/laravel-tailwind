<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Checkbox Switch
    </h2>

  @php
    $phpCode = '
    <?php

    namespace App\Http\Livewire;

    use Livewire\Component;

    class CheckboxSwitch extends Component
    {
      public $published;

      public function mount()
      {
        $this->published = 0;
      }

      public function render()
      {
        return view(\'livewire.checkbox-switch\');
      }
    }
    ';

    $bladeCode = '
      {{-- Add to style.css:

      @tailwind components;

      .toggle-checkbox:checked {
          @apply: right-0 border-green-600;
          right: 0;
          border-color: #43A047;
      }

      .toggle-checkbox:checked+.toggle-label {
          @apply: bg-green-600;
          background-color: #43A047;
      }

      } --}}

      <div class="pb-3">
      <div class="relative inline-block w-12 mr-2 align-middle select-none transition duration-200 ease-in">
        <input type="checkbox" name="published" id="published" wire:model="published"
          class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 border-red-600 appearance-none cursor-pointer" />
        <label for="published"
          class="toggle-label block overflow-hidden h-6 rounded-full bg-red-600 cursor-pointer"></label>
      </div>
      // <label for="published" class="{{ $published ? \'text-green-600\' : \'text-red-600\' }} uppercase pr-4">
        {{ $published ? \'Published\' : \'Not Published\' }}
      </label>
    </div>
    ';
    @endphp


    <div class="bg-gray-200 p-10 mb-10">
      <livewire:checkbox-switch />
    </div>

    <h4 class="text-xl font-nunito_bold text-blue-900 mb-6">app/Http/Livewire/CheckboxSwitch.php</h4>

    <pre><code class="php">{{ $phpCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">checkbox-switch.blade.php</h4>

    <pre><code class="html">{{ $bladeCode }}</code></pre>

  </div>

</x-layouts.app>
