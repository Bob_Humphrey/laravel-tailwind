<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <a href={{ url('/components/checkbox-switch') }}>
      <h2 class="w-full text-3xl font-nunito_bold text-blue-900 hover:text-blue-500 text-center pb-6">
        Checkbox Switch
      </h2>
    </a>

    <div class="bg-gray-200 p-10 mb-10">
      @livewire('checkbox-switch')
    </div>

    <a href={{ url('/components/dropdown') }}>
      <h2 class="w-full text-3xl font-nunito_bold text-blue-900 hover:text-blue-500 text-center pb-6">
        Dropdown Menu
      </h2>
    </a>

    <div class="bg-gray-200 p-10 mb-10">
      <x-misc-drop-down />
    </div>

    <a href={{ url('/components/pikaday') }}>
      <h2 class="w-full text-3xl font-nunito_bold text-blue-900 hover:text-blue-500 text-center pb-6">
        Pikaday
      </h2>
    </a>

    <div class="bg-gray-200 p-10 mb-10">
      @livewire('pikaday')
    </div>

    <a href={{ url('/components/radios') }}>
      <h2 class="w-full text-3xl font-nunito_bold text-blue-900 hover:text-blue-500 text-center pb-6">
        Radio Buttons
      </h2>
    </a>

    <div class="bg-gray-200 p-10 mb-10">
      @livewire('radios')
    </div>

    <a href={{ url('/components/searchbar') }}>
      <h2 class="w-full text-3xl font-nunito_bold text-blue-900 hover:text-blue-500 text-center pb-6">
        Search Bar
      </h2>
    </a>

    <div class="bg-gray-200 p-10 mb-10">
      @livewire('search-bar')
    </div>

    <a href={{ url('/components/trix') }}>
      <h2 class="w-full text-3xl font-nunito_bold text-blue-900 hover:text-blue-500 text-center pb-6">
        Trix Editor
      </h2>
    </a>

    @php
    $body = '';
    @endphp

    <div class="bg-gray-200 p-10 mb-10">
      <x-body-trix wire:model.lazy="body" id="body" class="bg-white p-6" :initial-value="$body" />
    </div>

  </div>

</x-layouts.app>
