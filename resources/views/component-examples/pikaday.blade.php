<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Pikaday
    </h2>

  @php
    $phpCode = '
      <?php

      namespace App\Http\Livewire;

      use App\Models\Entry;
      use Livewire\Component;
      use Illuminate\Support\Str;
      use Livewire\WithFileUploads;
      use Illuminate\Support\Facades\Log;
      use Intervention\Image\Facades\Image;

      class Add extends Component
      {
        use WithFileUploads;

        public $published_date;

        protected $rules = [
          \'published_date\' => \'date_format:m-d-Y\',
        ];

        public function add()
        {

          $this->validate();

          $formattedPublishedDate = substr($this->published_date, 6) . \'-\' . substr($this->published_date, 0, 2) . \'-\' . substr($this->published_date, 3, 2);


          $entry = Entry::create([
            \'published_date\' => $formattedPublishedDate,
          ]);

          session()->flash(\'success\', "New entry has been added: $this->title");
          return redirect("/add");
        }

        public function mount()
        {
        }


        public function render()
        {
          return view(\'livewire.add\');
        }
      }
    ';

    $bladeCode = '
      <div>
        <x-datepicker name="published_date" id="published_date" wire:model="published_date"
          class="p-2 rounded border border-gray-400 w-full appearance-none" placeholder="MM-DD-YYYY" />
      </div>
    ';

    $datepickerCode= '
    <div x-data x-init="new Pikaday({ field: $refs.input, format: \'MM-DD-YYYY\' })"
      @change="$dispatch(\'input\', $event.target.value)" class="">

      <input {{ $attributes }} x-ref="input" class="" />
    </div>
    ';
    @endphp


    <div class="bg-gray-200 p-10 mb-10">
      @livewire('pikaday')
    </div>

    <h4 class="text-xl font-nunito_bold text-blue-900 mb-6">Livewire Component</h4>

    <pre><code class="php">{{ $phpCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">View</h4>

    <pre><code class="html">{{ $bladeCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">components/datepicker.blade.php</h4>

    <pre><code class="html">{{ $datepickerCode }}</code></pre>

  </div>

</x-layouts.app>
