<div x-data="{ open: false }" class="relative">
  <div class="text-blue-900 hover:text-blue-500 divide-y-2 py-2" @click="open = true">
    TALL Stack Resources
  </div>
  <div x-show="open" @click.away="open = false" x-cloak
    class="absolute z-10 w-full bg-gray-50 text-base text-blue-800  border-t border-gray-100 rounded-t-none shadow-md">
    <div class="text-black  px-4">
      TAILWIND
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://tailwindcss.com/" target="_blank" rel="noopener noreferrer">
        Home
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://blog.tailwindcss.com/" target="_blank" rel="noopener noreferrer">
        Blog
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://nerdcave.com/tailwind-cheat-sheet" target="_blank" rel="noopener noreferrer">
        Cheat Sheet
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://daily-dev-tips.com/posts/tailwind-css-equal-height-columns/" target="_blank"
        rel="noopener noreferrer">
        Equal Height Columns
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://tailwindui.com/preview" target="_blank" rel="noopener noreferrer">
        Tailwind UI
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://www.youtube.com/c/TailwindLabs/videos" target="_blank" rel="noopener noreferrer">
        Tailwind Labs Videos
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://www.getrevue.co/profile/tailwind-weekly" target="_blank" rel="noopener noreferrer">
        Tailwind Weekly Newsletter
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://www.tailwindtoolbox.com/" target="_blank" rel="noopener noreferrer">
        Tailwind Toolbox
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://awesomeopensource.com/project/aniftyco/awesome-tailwindcss" target="_blank"
        rel="noopener noreferrer">
        Awesome Tailwind CSS
      </a>
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://www.tailwind-kit.com/" target="_blank" rel="noopener noreferrer">
        Tailwind UI Kit
      </a>
    </div>
    <div class="text-black  px-4">
      ALPINE
    </div>
    <div class=" px-4 hover:text-blue-500">
      <a href="https://www.alpinetoolbox.com/" target="_blank" rel="noopener noreferrer">
        Alpine Toolbox
      </a>
    </div>
  </div>
</div>
