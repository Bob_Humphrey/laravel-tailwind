@php
use Illuminate\Support\Str;
$path = request()->path();
$active = Str::of($path)->contains('form-examples') ? true : false;
@endphp

<div x-data="{ open: false }" class="relative">
  <div class="{{ $active ? 'text-blue-500' : 'text-blue-900' }} hover:text-blue-500 py-2" @click="open = true">
    Form Examples
  </div>
  <div x-show="open" @click.away="open = false" x-cloak
    class="absolute z-10 w-full bg-gray-50 text-base text-blue-800  border-t border-gray-100 rounded-t-none shadow-md">
    <div class="px-4 text-black">
      CRUD
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/form') }}">Form</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/add') }}">Add</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/update') }}">Update</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/delete') }}">Delete</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/view') }}">View</a>
    </div>
    <div class="px-4 text-black">
      INPUTS
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/text-input') }}">Text Input</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/text-input-with-errors') }}">Text Input with Errors</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/select-box') }}">Select Box</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/checkboxes') }}">Checkboxes</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/form-examples/radios') }}">Radios</a>
    </div>
  </div>
</div>
