<div x-data x-init="new Pikaday({ field: $refs.input, format: 'MM-DD-YYYY' })"
  @change="$dispatch('input', $event.target.value)" class="">

  <input {{ $attributes }} x-ref="input" class="" />
</div>
