@php
use Illuminate\Support\Str;
$path = request()->path();
$active = Str::of($path)->contains('misc/') ? true : false;
@endphp

<div x-data="{ open: false }" class="relative">
  <div class="{{ $active ? 'text-blue-500' : 'text-blue-900' }} hover:text-blue-500 py-2" @click="open = true">
    Miscellaneous
  </div>
  <div x-show="open" @click.away="open = false" x-cloak
    class="absolute z-10 w-full bg-gray-50 text-base text-blue-800  border-t border-gray-100 rounded-t-none shadow-md">
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/misc/flexbox-positioning') }}">Flexbox Positioning</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/misc/absolute-positioning') }}">Absolute Positioning</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/misc/object-fit') }}">Object Fit</a>
    </div>
    <div class="px-4 hover:text-blue-500">
      <a href="{{ url('/misc/object-position') }}">Object Position</a>
    </div>
  </div>
</div>
