<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Flexbox Positioning
    </h2>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center pb-6">
      Parent container setting its children along its cross axis
    </h3>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-start justify-center w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center py-6">
      items-start
    </h4>

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center py-6">
      items-center
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-center justify-center w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center py-6">
      items-end
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-end justify-center w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center py-6">
      items-stretch
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-stretch justify-center w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center py-6">
      items-baseline
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-baseline justify-center w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center py-6">
      Parent container setting its children along its main axis
    </h3>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-center justify-start w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center py-6">
      justify-start
    </h4>

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      justify-end
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-center justify-end w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      justify-center
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-center justify-center w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      justify-between
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-center justify-between w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      justify-around
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-center justify-around w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      justify-evenly
    </h4>

    @php
    $code = '
    <div class="flex flex-col lg:flex-row items-center justify-evenly w-full bg-gray-200 font-nunito_bold h-48">
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        One
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Two
      </a>
      <a class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
        href="/">
        Three
      </a>
    </div>
    ';
    @endphp

    <div class="mb-10">
      {!! $code !!}
    </div>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center pb-6">
      Child setting its own position along its parent container's cross axis
    </h3>

    @php
    $code = '
    <div class="flex items-center w-full font-nunito_regular bg-gray-200 h-72">
      <div class="flex self-start bg-gray-400 py-10 px-16 mx-auto" href="">
        One
      </div>
    </div>
    ';
    @endphp

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center py-6">
      self-start
    </h4>

    <div class="mb-10">
      {!! $code !!}
    </div>

    @php
    $code = '
    <div class="flex items-center w-full font-nunito_regular bg-gray-200 h-72">
      <div class="flex self-center bg-gray-400 py-10 px-16 mx-auto" href="">
        One
      </div>
    </div>
    ';
    @endphp

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      self-center
    </h4>

    <div class="mb-10">
      {!! $code !!}
    </div>

    @php
    $code = '
    <div class="flex items-center w-full font-nunito_regular bg-gray-200 h-72">
      <div class="flex self-end bg-gray-400 py-10 px-16 mx-auto" href="">
        One
      </div>
    </div>
    ';
    @endphp

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      self-end
    </h4>

    <div class="mb-10">
      {!! $code !!}
    </div>

    @php
    $code = '
    <div class="flex items-center w-full font-nunito_regular bg-gray-200 h-72">
      <div class="flex self-auto bg-gray-400 py-10 px-16 mx-auto" href="">
        One
      </div>
    </div>
    ';
    @endphp

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      self-auto
    </h4>

    <div class="mb-10">
      {!! $code !!}
    </div>

    @php
    $code = '
    <div class="flex items-center w-full font-nunito_regular bg-gray-200 h-72">
      <div class="flex self-stretch bg-gray-400 py-10 px-16 mx-auto" href="">
        One
      </div>
    </div>
    ';
    @endphp

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pb-6">
      self-stretch
    </h4>

    <div class="mb-10">
      {!! $code !!}
    </div>

  </div>

</x-layouts.app>
