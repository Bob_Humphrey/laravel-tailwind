<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Miscellaneous
    </h2>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center pb-6">
      Absolute Positioning
    </h3>

    @php
    $code = '
    <div class="relative w-full h-48 bg-gray-300">
      <div class="absolute bottom-5 left-5 bg-gray-600 text-white">
        <div class="p-12">Child</div>
      </div>
    </div>
    ';
    @endphp

    <div class="mb-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    @php
    $code = '
    <div class="relative w-full h-48 bg-gray-300">
      <div class="absolute inset-x-0 top-0 bg-gray-600 text-white">
        <div class="p-12">Child</div>
      </div>
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    @php
    $code = '
    <div class="relative w-full h-48 bg-gray-300">
      <div class="absolute inset-y-0 right-5 bg-gray-600 text-white">
        <div class="p-12">Child</div>
      </div>
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

  </div>

</x-layouts.app>
