<x-layouts.app>

  @php
  $url = asset('img/lisbon_5.jpg');
  @endphp

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Miscellaneous
    </h2>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center py-6">
      Object Fit
    </h3>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center">
      Contain
    </h4>

    @php
    $code = '
    <div class="w-full bg-gray-300">
      <img class="object-contain h-48 w-full" src="' . $url . '" loading="lazy" alt="Lisbon">
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Cover
    </h4>

    @php
    $code = '
    <div class="w-full bg-gray-300">
      <img class="object-cover h-48 w-full" src="' . $url . '" loading="lazy" alt="Lisbon">
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Fill
    </h4>

    @php
    $code = '
    <div class="w-full bg-gray-300">
      <img class="object-fill h-48 w-full" src="' . $url . '" loading="lazy" alt="Lisbon">
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      None
    </h4>

    @php
    $code = '
    <div class="w-none bg-gray-300">
      <img class="object-cover h-48 w-full" src="' . $url . '" loading="lazy" alt="Lisbon">
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

  </div>

</x-layouts.app>
