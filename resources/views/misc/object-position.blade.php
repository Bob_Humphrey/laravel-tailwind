<x-layouts.app>

  @php
  $url = asset('img/bh-logo-tiny.jpg');
  @endphp

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Miscellaneous
    </h2>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center py-6">
      Object Position
    </h3>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center">
      Left Top
    </h4>

    @php
    $code = '
    <img class="object-none object-left-top bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Top
    </h4>

    @php
    $code = '
    <img class="object-none object-top bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Right Top
    </h4>

    @php
    $code = '
    <img class="object-none object-right-top bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Left
    </h4>

    @php
    $code = '
    <img class="object-none object-left bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Center
    </h4>

    @php
    $code = '
    <img class="object-none object-center bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Right
    </h4>

    @php
    $code = '
    <img class="object-none object-right bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Left Bottom
    </h4>

    @php
    $code = '
    <img class="object-none object-left-bottom bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Bottom
    </h4>

    @php
    $code = '
    <img class="object-none object-bottom bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Right Bottom
    </h4>

    @php
    $code = '
    <img class="object-none object-right-bottom bg-gray-300 w-full h-36" src="' . $url . '" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

  </div>

</x-layouts.app>
