<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Error
    </h2>

    @php
    $code = '
    <x-error field="first_name">
      <ul>
        @foreach ($component->messages($errors) as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </x-error>
    ';
    @endphp

    <div class="my-6">
      <ul>
        <li>Incorrect first name.</li>
        <li>Needs at least 5 characters.</li>
      </ul>
    </div>


    <pre><code class="html">{{ $code }}</code></pre>

    <div class="pt-6">
      <a class="text-blue-900 hover:text-blue-500" href="https://blade-ui-kit.com/docs/0.x/error" target="_blank"
        rel="noopener noreferrer">
        Blade UI Kit Documentation
      </a>
    </div>

  </div>

</x-layouts.app>
