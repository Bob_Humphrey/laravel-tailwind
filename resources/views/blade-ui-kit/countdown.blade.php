<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Countdown
    </h2>

    @php
    $date = Illuminate\Support\Carbon::tomorrow();
    $code = '
    <x-countdown :expires="$date" />>
    ';
    @endphp

    <div class="md:flex my-6">
      Countdown to midnight: &nbsp;
      <x-countdown :expires="$date" />
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <div class="pt-6">
      <a class="text-blue-900 hover:text-blue-500" href="https://blade-ui-kit.com/docs/0.x/countdown" target="_blank"
        rel="noopener noreferrer">
        Blade UI Kit Documentation
      </a>
    </div>

  </div>

</x-layouts.app>
