<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Trix
    </h2>

    @php
    $code = '
    <div wire:ignore>
      <label class="text-gray-700 text-sm uppercase" for="body">
        Body
      </label>
      <trix-editor class="formatted-content" x-ref="trix" wire:model.debounce.999999ms="body" wire:key="uniqueKey">
      </trix-editor>
    </div>
    ';
    @endphp

    <div wire:ignore class="pb-6">
      <label class="text-gray-700 text-sm uppercase" for="body">
        Body
      </label>
      <trix-editor class="formatted-content" x-ref="trix" wire:model.debounce.999999ms="body" wire:key="uniqueKey">
      </trix-editor>
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <div class="pt-6">
      <a class="text-blue-900 hover:text-blue-500" href="https://blade-ui-kit.com/docs/0.x/trix" target="_blank"
        rel="noopener noreferrer">
        Blade UI Kit Documentation
      </a>
    </div>

  </div>

</x-layouts.app>
