<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      ToC
    </h2>

    @php
    $code = '
    <x-toc>
      # Hello World

      Blade UI components are **awesome**.

      ## Sub Title level 2

      Some content.

      ### Sub Sub Title level 3

      #### Sub Sub Title level 4

      Some content.

      ## Other Sub Title level 2

      Some content.
    </x-toc>
    ';
    @endphp

    <div class="my-6">
      <x-toc>
        # Hello World

        Blade UI components are **awesome**.

        ## Sub Title level 2

        Some content.

        ### Sub Sub Title level 3

        #### Sub Sub Title level 4

        Some content.

        ## Other Sub Title level 2

        Some content.
      </x-toc>
    </div>


    <pre><code class="html">{{ $code }}</code></pre>

    <div class="pt-6">
      <a class="text-blue-900 hover:text-blue-500" href="https://blade-ui-kit.com/docs/0.x/toc" target="_blank"
        rel="noopener noreferrer">
        Blade UI Kit Documentation
      </a>
    </div>

  </div>

</x-layouts.app>
