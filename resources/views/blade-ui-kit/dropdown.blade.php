<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Dropdown
    </h2>

    @php
    $code = '
    <x-dropdown class="text-gray-500">
      <x-slot name="trigger">
        <button>Dries</button>
      </x-slot>

      <a href="#">Profile</a>
      <a href="#">Settings</a>
      <a href="#">Logout</a>
    </x-dropdown>
    ';
    @endphp

    <div class="my-6">
      <x-dropdown class="text-gray-700">
        <x-slot name="trigger">
          <button>Dries</button>
        </x-slot>

        <a href="#">Profile</a>
        <a href="#">Settings</a>
        <a href="#">Logout</a>
      </x-dropdown>
    </div>


    <pre><code class="html">{{ $code }}</code></pre>

    <div class="pt-6">
      <a class="text-blue-900 hover:text-blue-700" href="https://blade-ui-kit.com/docs/0.x/dropdown" target="_blank"
        rel="noopener noreferrer">
        Blade UI Kit Documentation
      </a>
    </div>

  </div>

</x-layouts.app>
