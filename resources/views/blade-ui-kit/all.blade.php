<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto py-12">

    <div class="lg:grid grid-cols-5 text-xl">

      {{-- COLUMN 1 --}}

      <div class="mb-6">

        {{-- ALERTS --}}

        <h4 class="text-gray-700 mb-2">
          ALERTS
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/alert">
              Alert
            </a>
          </li>
        </ul>

        {{-- BUTTONS --}}

        <h4 class="text-gray-700 mb-2">
          BUTTONS
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/form-button">
              Form Button
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/logout">
              Logout
            </a>
          </li>
        </ul>

        {{-- DATETIME --}}

        <h4 class="text-gray-700 mb-2">
          DATETIME
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/carbon">
              Carbon
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/countdown">
              Countdown
            </a>
          </li>
        </ul>

      </div>

      {{-- COLUMN 2 --}}

      <div class="mb-6">

        {{-- EDITORS --}}

        <h4 class="text-gray-700 mb-2">
          EDITORS
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/easy-mde">
              Easy MDE
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/trix">
              Trix
            </a>
          </li>
        </ul>

        {{-- FORMS --}}

        <h4 class="text-gray-700 mb-2">
          FORMS
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/error">
              Error
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/form">
              Form
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/label">
              Label
            </a>
          </li>
        </ul>

      </div>

      {{-- COLUMN 3 --}}

      <div class="mb-6">

        {{-- INPUTS --}}

        <h4 class="text-gray-700 mb-2">
          INPUTS
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/checkbox">
              Checkbox
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/color-picker">
              Color Picker
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/email">
              Email
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/input">
              Input
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/password">
              Password
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/pikaday">
              Pikaday
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/textarea">
              Textarea
            </a>
          </li>
        </ul>

      </div>

      {{-- COLUMN 4 --}}

      <div class="mb-6">

        {{-- LAYOUTS --}}

        <h4 class="text-gray-700 mb-2">
          LAYOUTS
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/html">
              HTML
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/social-meta">
              Social Meta
            </a>
          </li>
        </ul>

        {{-- MAPS --}}

        <h4 class="text-gray-700 mb-2">
          MAPS
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/mapbox">
              Mapbox
            </a>
          </li>
        </ul>

        {{-- MARKDOWN --}}

        <h4 class="text-gray-700 mb-2">
          MARKDOWN
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/markdown">
              Markdown
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/toc">
              ToC
            </a>
          </li>
        </ul>

      </div>

      {{-- COLUMN 5 --}}

      <div class="mb-6">

        {{-- NAVIGATION --}}

        <h4 class="text-gray-700 mb-2">
          NAVIGATION
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/dropdown">
              Dropdown
            </a>
          </li>
        </ul>

        {{-- SUPPORT --}}

        <h4 class="text-gray-700 mb-2">
          SUPPORT
        </h4>

        <ul class="mb-6">
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/avatar">
              Avatar
            </a>
          </li>
          <li>
            <a class="text-blue-900 hover:text-blue-500" href="blade-ui-kit/unsplash">
              Unsplash
            </a>
          </li>
        </ul>

      </div>

    </div>

  </div>

</x-layouts.app>
