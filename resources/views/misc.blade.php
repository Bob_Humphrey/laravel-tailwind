<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Miscellaneous
    </h2>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center pb-6">
      Absolute Positioning
    </h3>

    @php
    $code = '
    <div class="relative w-full h-48 bg-gray-300">
      <div class="absolute bottom-5 left-5 bg-gray-600 text-white">
        <div class="p-12">Child</div>
      </div>
    </div>
    ';
    @endphp

    <div class="mb-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    @php
    $code = '
    <div class="relative w-full h-48 bg-gray-300">
      <div class="absolute inset-x-0 top-0 bg-gray-600 text-white">
        <div class="p-12">Child</div>
      </div>
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    @php
    $code = '
    <div class="relative w-full h-48 bg-gray-300">
      <div class="absolute inset-y-0 right-5 bg-gray-600 text-white">
        <div class="p-12">Child</div>
      </div>
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center py-6">
      Object Fit
    </h3>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center">
      Contain
    </h4>

    @php
    $code = '
    <div class="w-full bg-gray-300">
      <img class="object-contain h-48 w-full" src="img/lisbon_5.jpg" loading="lazy" alt="Lisbon">
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Cover
    </h4>

    @php
    $code = '
    <div class="w-full bg-gray-300">
      <img class="object-cover h-48 w-full" src="img/lisbon_5.jpg" loading="lazy" alt="Lisbon">
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Fill
    </h4>

    @php
    $code = '
    <div class="w-full bg-gray-300">
      <img class="object-fill h-48 w-full" src="img/lisbon_5.jpg" loading="lazy" alt="Lisbon">
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      None
    </h4>

    @php
    $code = '
    <div class="w-none bg-gray-300">
      <img class="object-cover h-48 w-full" src="img/lisbon_5.jpg" loading="lazy" alt="Lisbon">
    </div>
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h3 class="w-full text-2xl font-nunito_bold text-red-900 text-center py-6">
      Object Position
    </h3>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center">
      Left Top
    </h4>

    @php
    $code = '
    <img class="object-none object-left-top bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy"
      alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Top
    </h4>

    @php
    $code = '
    <img class="object-none object-top bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Right Top
    </h4>

    @php
    $code = '
    <img class="object-none object-right-top bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy"
      alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Left
    </h4>

    @php
    $code = '
    <img class="object-none object-left bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Center
    </h4>

    @php
    $code = '
    <img class="object-none object-center bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Right
    </h4>

    @php
    $code = '
    <img class="object-none object-right bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Left Bottom
    </h4>

    @php
    $code = '
    <img class="object-none object-left-bottom bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy"
      alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Bottom
    </h4>

    @php
    $code = '
    <img class="object-none object-bottom bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy" alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

    <h4 class="w-full text-xl font-nunito_bold text-blue-900 text-center pt-6">
      Right Bottom
    </h4>

    @php
    $code = '
    <img class="object-none object-right-bottom bg-gray-300 w-full h-36" src="img/bh-logo-tiny.jpg" loading="lazy"
      alt="logo">
    ';
    @endphp

    <div class="my-6">
      {!! $code !!}
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

  </div>

</x-layouts.app>
