@php
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
$path = request()->path();
$home = Str::of($path)->exactly('/') ? true : false;
$all = Str::of($path)->exactly('all') ? true : false;
$bladeUIKit = Str::of($path)->contains('blade-ui-kit') ? true : false;
$components = Str::of($path)->contains('components') ? true : false;
$flexboxPositioning = Str::of($path)->contains('flexbox-positioning') ? true : false;
@endphp

<div class="flex flex-col xl:flex-row xl:space-x-6">

  <a href="{{ url('/all') }}" class="{{ $all ? 'text-blue-500' : '' }} hover:text-blue-500 py-2">
    Patterns
  </a>
  <a href="{{ url('blade-ui-kit') }}" class="{{ $bladeUIKit ? 'text-blue-500' : '' }} hover:text-blue-500 py-2">
    Blade UI Kit
  </a>
  <a href="{{ url('components') }}" class="{{ $components ? 'text-blue-500' : '' }} hover:text-blue-500 py-2">
    Components
  </a>
  <div>
    <x-forms-drop-down />
  </div>
  <div>
    <x-misc-drop-down />
  </div>
  <div>
    <x-tailwind-drop-down />
  </div>

</div>
