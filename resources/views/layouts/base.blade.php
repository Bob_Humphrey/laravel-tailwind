<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description"
    content="An application to collect patterns and components used to build web application interfaces. Built with the TALL stack--Tailwind CSS, Alpine JS, Laravel and Livewire.">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  @bukStyles(true)
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">

</head>

<body>

  {{ $slot }}

  <!-- Scripts -->

  <script src="{{ asset('js/all.js') }}" defer></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>
    hljs.initHighlightingOnLoad();

  </script>

  @livewireScripts
  @bukScripts(true)

</body>

</html>
