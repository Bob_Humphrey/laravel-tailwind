<!-- Source: https://laravel-news.com/tailwind-css-tips-and-tricks -->
<!-- The negative margin -mx-4 in the row container must match the horizontal padding p-4 in the column containers. -->
<!-- Container -->
<div class="max-w-screen-xl mx-auto bg-gray-200 p-6 lg:p-32">
  <!-- Row -->
  <div class="-mx-4 flex flex-wrap">
    <!-- Grid column -->
    <div class="w-full flex flex-col p-4 xl:w-1/3">
      <!-- Column contents -->
      <div class="flex flex-col flex-1 px-10 py-12 bg-white rounded-lg shadow-lg">
        <div class="flex-1">
          <h2 class="text-gray-900 text-lg font-bold leading-snug">
            Tailwind v1.1.0
          </h2>
          <p class="mt-2 text-base">
            Tailwind v1.1.0 has been released with some cool new features and a
            couple of bug fixes. This is the first feature release since the
            v1.0 release, so let's dig into some of the updates.
          </p>
        </div>
        <a href="#" class="mt-6 inline-flex items-center justify-center px-6 py-3 text-white font-semibold bg-gray-700 hover:bg-black rounded-md shadow-sm">
          View article
        </a>
      </div>
    </div>
    <!-- Grid column -->
    <div class="w-full flex flex-col p-4 xl:w-1/3">
      <!-- Column contents -->
      <div class="flex flex-col flex-1 px-10 py-12 bg-white rounded-lg shadow-lg">
        <div class="flex-1">
          <h2 class="text-gray-900 text-lg font-bold leading-snug">
            Getting Started with Tailwind CSS Custom Forms
          </h2>
          <p class="mt-2 text-base">
            In this tutorial, I show you how to install the Tailwind CSS Custom
            Forms plugin and get started using it.
          </p>
        </div>
        <a href="#" class="mt-6 inline-flex items-center justify-center px-6 py-3 text-white font-semibold bg-gray-700 hover:bg-black rounded-md shadow-sm">
          View article
        </a>
      </div>
    </div>
    <!-- Grid column -->
    <div class="w-full flex flex-col p-4 xl:w-1/3">
      <!-- Column contents -->
      <div class="flex flex-col flex-1 px-10 py-12 bg-white rounded-lg shadow-lg">
        <div class="flex-1">
          <h2 class="text-gray-900 text-lg font-bold leading-snug">
            11 Benefits of Tailwind CSS
          </h2>
          <p class="mt-2 text-base">
            I've been using Tailwind CSS professionally almost every day for
            nearly two years. Here I share some of the benefits I've gained by
            using Tailwind.
          </p>
        </div>
        <a href="#" class="mt-6 inline-flex items-center justify-center px-6 py-3 text-white font-semibold bg-gray-700 hover:bg-black rounded-md shadow-sm">
          View article
        </a>
      </div>
    </div>
  </div>
</div>