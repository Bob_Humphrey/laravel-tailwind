<div class="flex w-full items-center justify-center bg-gray-300 font-nunito_black text-7xl h-48">
  <span class="text-transparent bg-clip-text bg-gradient-to-r from-green-400 to-blue-500">
    Start Here
  </span>
</div>