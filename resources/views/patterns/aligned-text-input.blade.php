<div class="flex justify-center w-full bg-gray-200 py-6">
  <form class="bg-gray-300 w-4/5 lg:w-2/3 font-nunito_regular pt-6 pb-12 lg:pb-6 px-8 lg:px-24" action="">
    <div class="grid lg:grid-cols-3 mb-4 -mx-2">
      <div class="col-span-1">
        <label class="block py-2" for="first_name">
          First Name
        </label>
      </div>
      <div class="col-span-2 bg-white py-2 px-4 rounded">
        <input class="w-full bg-white" id="first_name" name="first_name" type="text" value="" />
      </div>
    </div>
    <div class="grid lg:grid-cols-3 -mx-2">
      <div class="col-span-1">
        <label class="block py-2" for="first_name">
          Last Name
        </label>
      </div>
      <div class="col-span-2 bg-white py-2 px-4 rounded">
        <input class="w-full bg-white" id="last_name" name="last_name" type="text" value="" />
      </div>
    </div>
  </form>
</div>