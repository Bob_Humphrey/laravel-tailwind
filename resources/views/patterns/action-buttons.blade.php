{{-- Add to style.css: 

.link {
    @apply text-blue-900 hover:text-blue-500 cursor-pointer;
}

.tooltip .tooltip-text {
    @apply invisible px-4 py-1 absolute z-50 inline-block -ml-4 -mt-8 text-sm rounded bg-gray-300 text-black;
}

.tooltip:hover .tooltip-text {
    @apply visible;
} --}}

<div>
  <table class="w-36 mx-auto">
    <tr class="">
      <td class="flex items-center justify-around text-left py-3">
        <div class="tooltip">
          <span class="tooltip-text">View</span>
          <a href='#'>
            <x-heroicon-o-eye class="w-7 link" />
          </a>
        </div>
        <div class="tooltip">
          <span class="tooltip-text">Edit</span>
          <a href='#'>
            <x-zondicon-edit-pencil class="w-5 link" />
          </a>
        </div>
        <div class="tooltip">
          <span class="tooltip-text">Delete</span>
          <a href='#'>
            <x-zondicon-close-outline class="w-5 link" />
          </a>
        </div>
      </td>
    </tr>
  </table>
</div>
