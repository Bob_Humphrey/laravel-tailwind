<div class="flex flex-col lg:flex-row items-center lg:justify-between w-full 
bg-gray-200 font-nunito_bold lg:h-16 lg:px-16 py-12 lg:py-0">
  <div class="lg:justify-start text-3xl font-nunito_bold">
    <a class="" href="/">
      Heading
    </a>
  </div>
  <div class="lg:justify-end text-2xl font-nunito_light flex flex-col lg:flex-row lg:space-x-6">
    <a class="" href="/">
      Link
    </a>
    <a class="" href="/">
      Link
    </a>
    <a class="" href="/">
      Link
    </a>
  </div>
</div>