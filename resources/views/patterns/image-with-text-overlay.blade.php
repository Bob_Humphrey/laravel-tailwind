<div class="relative w-full h-48 lg:w-3/4 lg:h-96 mx-auto overflow-hidden">
  <img class="absolute inset-0" src="img/lisbon_5.jpg" loading="lazy" alt="Lisbon">
  <div class="absolute inset-0 bg-gray-900 bg-opacity-30"></div>
  <div class="relative flex items-center justify-center h-full">
    <div class="text-6xl font-nunito_black text-white">Lisbon</div>
  </div>
</div>