<div class="flex w-full justify-center bg-gray-200 py-6">
  <table class="w-11/12 lg:w-2/3">
    <tr class="grid grid-cols-12 text-sm font-nunito_bold border-b border-gray-300">
      <th class="col-span-4 text-left py-3">Team</th>
      <th class="col-span-2 text-right py-3">W</th>
      <th class="col-span-2 text-right py-3">L</th>
      <th class="col-span-2 text-right py-3">Pct</th>
      <th class="col-span-2 text-right py-3">GB</th>
    </tr>
    <tr class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300">
      <td class="col-span-4 py-3">Twins</td>
      <td class="col-span-2 text-right py-3">101</td>
      <td class="col-span-2 text-right py-3">61</td>
      <td class="col-span-2 text-right py-3">.623</td>
      <td class="col-span-2 text-right py-3">-</td>
    </tr>
    <tr class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300">
      <td class="col-span-4 py-3">Indians</td>
      <td class="col-span-2 text-right py-3">93</td>
      <td class="col-span-2 text-right py-3">69</td>
      <td class="col-span-2 text-right py-3">.574</td>
      <td class="col-span-2 text-right py-3">8.0</td>
    </tr>
  </table>
</div>