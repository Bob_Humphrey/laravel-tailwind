<div class="flex flex-col lg:flex-row items-center justify-center w-full bg-gray-200 font-nunito_bold h-48 lg:space-x-10">
  <a class="bg-blue-200 text-blue-900 hover:ring hover:ring-blue-300 hover:ring-offset-4 hover:ring-offset-gray-200 text-center rounded py-2 mb-2 lg:mb-0 w-48 " href="/">
    One
  </a>
  <a class="bg-blue-200 text-blue-900 hover:outline-none hover:ring hover:ring-blue-300 hover:ring-offset-4 hover:ring-offset-gray-200 text-center rounded py-2 mb-2 lg:mb-0 w-48" href="/">
    Two
  </a>
  <a class="bg-blue-200 text-blue-900 hover:outline-none hover:ring hover:ring-blue-300 hover:ring-offset-4 hover:ring-offset-gray-200 space-y-6 text-center rounded py-2 mb-2 lg:mb-0 w-48" href="/">
    Three
  </a>
</div>