<div class="flex w-full font-nunito_light justify-center bg-gray-200 pt-6 pb-4 space-x-5">
  <div class="bg-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mb-2">
    All
  </div>
  <div class="bg-gray-700 text-white text-sm text-center rounded-full cursor-pointer px-2 mb-2">
    Open
  </div>
  <div class="bg-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mb-2">
    Closed
  </div>
</div>