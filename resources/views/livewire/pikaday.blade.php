<div>
  <x-datepicker name="date" id="date" wire:model="date"
    class="p-2 rounded border border-gray-400 w-full appearance-none" placeholder="MM-DD-YYYY" />
</div>
