<div>
  <div class="text-xl mb-4">
    Size: {{ $size }}
  </div>

  <div class="flex space-x-6">
    <div>
      Select the size:
    </div>
    <div>
      <span>Small</span>
      <x-input type="radio" name="size" value="Small" wire:model="size" />
    </div>
    <div>
      <span>Medium</span>
      <x-input type="radio" name="size" value="Medium" wire:model="size" />
    </div>
    <div>
      <span>Large</span>
      <x-input type="radio" name="size" value="Large" wire:model="size" />
    </div>
  </div>
</div>
