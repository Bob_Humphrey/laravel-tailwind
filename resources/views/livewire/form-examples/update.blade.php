@include('livewire.form-examples.form', ['disabled' => ''])

{{-- SUBMIT BUTTON --}}

<div class="flex justify-center pt-3">
  <button type="submit" wire:click="update"
    class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
    Update
  </button>
</div>
