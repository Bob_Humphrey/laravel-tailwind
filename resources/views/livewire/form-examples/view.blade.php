@include('livewire.form-examples.form', ['disabled' => 'disabled'])

{{-- LINK BUTTONS --}}

<div class="flex justify-center pt-3">
  <a href={{ url('#') }}
    class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
    Update
  </a>
  <a href={{ url('#') }}
    class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
    Delete
  </a>
</div>
