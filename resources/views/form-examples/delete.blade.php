@php
$disabled = 'disabled';
$states = App\Models\State::all();
$componentCode = '
<?php

namespace App\Http\Livewire\Areas;

use App\Area;
use App\Models\State;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Delete extends Component
{
  use AuthorizesRequests;

  public $area;
  public $area_id;
  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $description;
  // States
  public $states;
  public $stateName;
  // Attractions
  public $attractions;
  // Lodging
  public $lodgings;
  // Can delete this record
  public $canDelete;

  public function delete()
  {
    $this->authorize(\'edit\');

    $this->area->delete();
    session()->flash(\'success\', "Area has been deleted: $this->name");
    return redirect("/areas");
  }

  public function mount(Request $request)
  {
    $this->states = State::get();
    $area = Area::find($request->id);
    $this->area_id = $request->id;
    $this->area = $area;
    $this->name = $area->name;
    $this->address = $area->address;
    $this->lat = $area->lat;
    $this->lng = $area->lng;
    $this->state = $area->state;
    $this->description = $area->description;
    $state = State::where(\'abbreviation\', $area->state)->first();
    $this->stateName = $state->name;
    $this->attractions = $area->attractions->all();
    $this->lodgings = $area->lodgings->all();
    $this->canDelete = true;
    $this->canDelete = count($this->attractions) ? false : $this->canDelete;
    $this->canDelete = count($this->lodgings) ? false : $this->canDelete;
  }

  public function render()
  {
    return view(\'livewire.areas.delete\');
  }
}
';
$viewCode = '
<div class="w-3/4 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Delete Area
  </h2>

  @if (count($attractions))

    <div class="w-1/2 mb-12 mx-auto">
      <div class="font-nunito_bold mb-4">
        Cannot delete {{ $name }} because there are attractions associated with it.
      </div>

      @foreach ($attractions as $attraction)
        <div class="">
          <a href="{{ url(\'attractions/\' . $attraction->id) }}" class="text-blue-500">
            {{ $attraction->name }}
          </a>
        </div>
      @endforeach
    </div>

  @endif

  @if (count($lodgings))

    <div class="w-1/2 mb-12 mx-auto">
      <div class="font-nunito_bold mb-4">
        Cannot delete {{ $name }} because there are lodgings associated with it.
      </div>

      @foreach ($lodgings as $lodging)
        <div class="">
          <a href="{{ url(\'lodgings/\' . $lodging->id) }}" class="text-blue-500">
            {{ $lodging->name }}
          </a>
        </div>
      @endforeach
    </div>

  @endif

  @if ($canDelete)

    <div class="bg-gray-50 p-16 rounded border border-gray-200 mb-12">

      <div class="font-nunito_bold text-red-700 mb-4">
        Warning: This will permanently delete the area!
      </div>

      @include(\'livewire.areas.form\', [\'disabled\' => \'disabled\'])

      {{-- SUBMIT BUTTON --}}

      <div class="flex justify-center pt-3">
        <button type="submit" wire:click="delete"
          class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
          Delete
        </button>
      </div>

    </div>

  @endif

</div>

';
@endphp

<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      CRUD Delete
    </h2>

    @include('livewire.form-examples.delete')

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">Livewire/Delete.php</h4>

    <pre><code class="html">{{ $componentCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">livewire/delete.blade.php</h4>

    <pre><code class="html">{{ $viewCode }}</code></pre>

  </div>

</x-layouts.app>
