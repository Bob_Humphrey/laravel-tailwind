<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Checkboxes
    </h2>

    @php
    $code = '
    <x-form action="" class="w-full lg:w-1/2 mb-6">
      @foreach ($sizes as $size)
        <div class="">
          @if ($disabled)
            <x-checkbox name="size" wire:model="{{ $size }}" disabled />
          @else
            <x-checkbox name="size" wire:model="{{ $size }}" />
          @endif
          {{ $size }}
        </div>
      @endforeach
    </x-form>
    ';
    $disabled = '';
    $sizes = ['Small', 'Medium', 'Large', 'Extra Large'];
    @endphp

    <x-form action="" class="w-full lg:w-1/2 mb-6">
      @foreach ($sizes as $size)
        <div class="">
          @if ($disabled)
            <x-checkbox name="size" wire:model="{{ $size }}" disabled />
          @else
            <x-checkbox name="size" wire:model="{{ $size }}" />
          @endif
          {{ $size }}
        </div>
      @endforeach
    </x-form>

    <pre><code class="html">{{ $code }}</code></pre>

  </div>

</x-layouts.app>
