<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Text Input
    </h2>

    @php
    $code = '
    <x-form action="" class="w-full lg:w-1/2 mb-6">
      <div class="grid grid-cols-12 gap-x-6 pb-2">
        <div class="col-span-6">
          <x-label for="first_name" class="w-full" />
          <x-input name="first_name" class="w-full p-2 border border-gray-400 rounded" />
        </div>
        <div class="col-span-6">
          <x-label for="last_name" class="w-full" />
          <x-input name="last_name" class="w-full p-2 border border-gray-400 rounded" />
        </div>
      </div>
      <div class="pb-2">
        <x-label for="street_address" class="w-full" />
        <x-input name="street_address" class="w-full p-2 border border-gray-400 rounded" />
      </div>
      <div class="grid grid-cols-12 gap-x-6 pb-2">
        <div class="col-span-7">
          <x-label for="city" class="w-full" />
          <x-input name="city" class="w-full p-2 border border-gray-400 rounded" />
        </div>
        <div class="col-span-2">
          <x-label for="state" class="w-full" />
          <x-input name="state" class="w-full p-2 border border-gray-400 rounded" />
        </div>
        <div class="col-span-3">
          <x-label for="zip_code" class="w-full" />
          <x-input name="zip_code" class="w-full p-2 border border-gray-400 rounded" />
        </div>
      </div>
    </x-form>
    ';
    @endphp

    <x-form action="" class="w-full lg:w-1/2 mb-6">
      <div class="grid grid-cols-12 gap-x-6 pb-2">
        <div class="col-span-6">
          <x-label for="first_name" class="w-full" />
          <x-input name="first_name" class="w-full p-2 border border-gray-400 rounded" />
        </div>
        <div class="col-span-6">
          <x-label for="last_name" class="w-full" />
          <x-input name="last_name" class="w-full p-2 border border-gray-400 rounded" />
        </div>
      </div>
      <div class="pb-2">
        <x-label for="street_address" class="w-full" />
        <x-input name="street_address" class="w-full p-2 border border-gray-400 rounded" />
      </div>
      <div class="grid grid-cols-12 gap-x-6 pb-2">
        <div class="col-span-7">
          <x-label for="city" class="w-full" />
          <x-input name="city" class="w-full p-2 border border-gray-400 rounded" />
        </div>
        <div class="col-span-2">
          <x-label for="state" class="w-full" />
          <x-input name="state" class="w-full p-2 border border-gray-400 rounded" />
        </div>
        <div class="col-span-3">
          <x-label for="zip_code" class="w-full" />
          <x-input name="zip_code" class="w-full p-2 border border-gray-400 rounded" />
        </div>
      </div>
    </x-form>

    <pre><code class="html">{{ $code }}</code></pre>

  </div>

</x-layouts.app>
