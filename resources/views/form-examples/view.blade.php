@php
$disabled = 'disabled';
$states = App\Models\State::all();
$componentCode = '
<?php

namespace App\Http\Livewire\Areas;

use App\Area;
use App\Models\State;
use Livewire\Component;
use Illuminate\Http\Request;

class View extends Component
{
  public $area;
  public $area_id;
  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $description;
  // States
  public $states;
  public $stateName;
  // Attractions
  public $attractions;
  // Lodging
  public $lodgings;

  public function mount(Request $request)
  {
    $this->states = State::get();
    $area = Area::find($request->id);
    $this->area_id = $request->id;
    $this->area = $area;
    $this->name = $area->name;
    $this->address = $area->address;
    $this->lat = $area->lat;
    $this->lng = $area->lng;
    $this->state = $area->state;
    $this->description = $area->description;
    $state = State::where(\'abbreviation\', $area->state)->first();
    $this->stateName = $state->name;
    $this->attractions = $area->attractions->sortBy(\'name\');
    $this->lodgings = $area->lodgings;
  }

  public function render()
  {
    return view(\'livewire.areas.view\');
  }
}
';
$viewCode = '
<div class="w-2/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-8 text-center">
    {{ $name }}
  </h2>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-mail />
      </div>
    </div>
    <div class="col-span-11">
      {{ $address }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-at-symbol />
      </div>
    </div>
    <div class="col-span-11">
      {{ $stateName }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-location-marker />
      </div>
    </div>
    <div class="col-span-11">
      {{ $lat }}, {{ $lng }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-exclamation-circle />
      </div>
    </div>
    <div class="col-span-11">
      @foreach ($attractions as $attraction)
        <div class="">
          <a href="{{ url(\'attractions/\' . $attraction->id) }}" class="text-blue-500">
            {{ $attraction->name }}
          </a>
        </div>
      @endforeach
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-office-building />
      </div>
    </div>
    <div class="col-span-11">
      @foreach ($lodgings as $lodging)
        <div class="">
          <a href="{{ url(\'lodgings/\' . $lodging->id) }}" class="text-blue-500">
            {{ $lodging->name }}
          </a>
        </div>
      @endforeach
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-annotation />
      </div>
    </div>
    <div class="col-span-11">
      {!! nl2br($description) !!}
    </div>
  </div>

  <div class="flex justify-center pt-3">
    <a href={{ url("areas/$area_id/update") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Update
    </a>
    <a href={{ url("areas/$area_id/delete") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Delete
    </a>
  </div>

</div>
';
@endphp

<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      CRUD View
    </h2>

    @include('livewire.form-examples.view')

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">Livewire/View.php</h4>

    <pre><code class="html">{{ $componentCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">livewire/view.blade.php</h4>

    <pre><code class="html">{{ $viewCode }}</code></pre>

  </div>

</x-layouts.app>
