@php
$disabled = '';
$states = App\Models\State::all();
$componentCode = '
<?php

namespace App\Http\Livewire\Areas;

use App\Area;
use App\Models\State;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Add extends Component
{
  use AuthorizesRequests;

  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $description;
  // States
  public $states;

  protected $rules = [
    \'name\' => \'required|string|min:3\',
    \'address\' => \'nullable|string\',
    \'state\' => \'required|string\',
    \'description\' => \'nullable|string\',
  ];

  public function add()
  {
    $this->authorize(\'edit\');

    $this->validate();

    $area = Area::create([
      \'name\' => Str::title($this->name),
      \'address\' => $this->address,
      \'state\' => $this->state,
      \'description\' => $this->description,
    ]);

    session()->flash(\'success\', "New areas has been added: $this->name");
    return redirect("/areas");
  }

  public function mount()
  {
    $this->states = State::get();
  }

  public function render()
  {
    return view(\'livewire.areas.add\');
  }
}

';
$viewCode = '
<div class="w-3/4 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Add Area
  </h2>

  <div class="bg-gray-50 p-16 rounded border border-gray-200">

    @include(\'livewire.areas.form\', [\'disabled\' => \'\'])

    {{-- SUBMIT BUTTON --}}

    <div class="flex justify-center pt-3">
      <button type="submit" wire:click="add"
        class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
        Add
      </button>
    </div>

  </div>

</div>
';
@endphp

<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      CRUD Add
    </h2>

    @include('livewire.form-examples.add')

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">Livewire/Add.php</h4>

    <pre><code class="html">{{ $componentCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">livewire/add.blade.php</h4>

    <pre><code class="html">{{ $viewCode }}</code></pre>

  </div>

</x-layouts.app>
