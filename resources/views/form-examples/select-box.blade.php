<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      Select Box
    </h2>

    @php
    $code = '
    <x-form action="" class="w-full lg:w-1/2 mb-6">
      {{-- STATE --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="state" />
          </div>

          <div class="">
            <select name="state" id="state" wire:model="state"
              class="p-2 rounded border border-gray-200 w-full appearance-none" {{ $disabled }}>
              <option value="">Pick a state...</option>
              @foreach ($states as $key => $value)
                <option value={{ $value[\'stateabbreviation\'] }}>{{ $value[\'name\'] }}</option>
              @endforeach
            </select>
          </div>

          <div class="">
            <x-error field="state" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>

        </div>
    </x-form>
    ';
    $disabled = '';
    $states = App\Models\State::all();
    @endphp

    <x-form action="" class="w-full lg:w-1/2 mb-6">
      {{-- STATE --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="state" />
          </div>

          <div class="">
            <select name="state" id="state" wire:model="state"
              class="p-2 rounded border border-gray-200 w-full appearance-none" {{ $disabled }}>
              <option value="">Pick a state...</option>
              @foreach ($states as $key => $value)
                <option value={{ $value['abbreviation'] }}>{{ $value['name'] }}</option>
              @endforeach
            </select>
          </div>

          <div class="">
            <x-error field="state" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>

        </div>
    </x-form>

    <pre><code class="html">{{ $code }}</code></pre>

  </div>

</x-layouts.app>
