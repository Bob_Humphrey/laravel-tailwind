@php
$disabled = '';
$states = App\Models\State::all();
$componentCode = '
<?php

namespace App\Http\Livewire\Areas;

use App\Area;
use App\Models\State;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Update extends Component
{
  use AuthorizesRequests;

  public $area;
  public $area_id;
  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $description;
  // States
  public $states;

  protected $rules = [
    \'name\' => \'required|string|min:3\',
    \'address\' => \'nullable|string\',
    \'state\' => \'required|string\',
    \'description\' => \'nullable|string\',
  ];

  public function update()
  {
    $this->authorize(\'edit\');

    $this->validate();

    $this->area->name = Str::title($this->name);
    $this->area->address = $this->address;
    $this->area->state = $this->state;
    $this->area->description = $this->description;
    $this->area->save();

    session()->flash(\'success\', "Area has been updated: $this->name");
     return redirect("/areas");
  }

  public function mount(Request $request)
  {
    $this->states = State::get();
    $area = Area::find($request->id);
    $this->area_id = $request->id;
    $this->area = $area;
    $this->name = $area->name;
    $this->address = $area->address;
    $this->lat = $area->lat;
    $this->lng = $area->lng;
    $this->state = $area->state;
    $this->description = $area->description;
  }

  public function render()
  {
    return view(\'livewire.areas.update\');
  }
}

';
$viewCode = '
<div class="w-3/4 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Update Area
  </h2>

  <div class="bg-gray-50 p-16 rounded border border-gray-200">

    @include(\'livewire.areas.form\', [\'disabled\' => \'\'])

    {{-- SUBMIT BUTTON --}}

    <div class="flex justify-center pt-3">
      <button type="submit" wire:click="update"
        class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
        Update
      </button>
    </div>

  </div>

</div>
';
@endphp

<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      CRUD Update
    </h2>

    @include('livewire.form-examples.update')

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">Livewire/Update.php</h4>

    <pre><code class="html">{{ $componentCode }}</code></pre>

    <h4 class="text-xl font-nunito_bold text-blue-900 my-6">livewire/update.blade.php</h4>

    <pre><code class="html">{{ $viewCode }}</code></pre>

  </div>

</x-layouts.app>
