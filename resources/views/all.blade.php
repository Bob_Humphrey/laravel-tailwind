<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    @foreach ($patterns as $key => $value)

      <a href={{ url('/' . $key) }}>
        <h2 class="w-full text-3xl font-nunito_bold text-blue-900 hover:text-blue-500 text-center pb-6">
          {{ $value['patternName'] }}
        </h2>
      </a>

      {{-- <div class="mb-10">
        {!!  $value['code'] !!}
      </div> --}}

      <div class="mb-10">
        @include('patterns.' . $key)
      </div>

    @endforeach

  </div>

</x-layouts.app>
