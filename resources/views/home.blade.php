<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <div class="lg:grid grid-cols-3 w-full text-xl py-16">

      @foreach ($chunks as $chunk)

        <div class="">

          @foreach ($chunk as $key => $value)

            <div class="text-black hover:text-blue-500 mb-3 ">
              <a href={{ '/' . $key }}> {{ $value }} </a>
            </div>

          @endforeach

        </div>

      @endforeach

    </div>

  </div>

</x-layouts.app>
