<x-layouts.app>

  <div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

    <h2 class="w-full text-3xl font-nunito_bold text-blue-900 text-center pb-6">
      {{ $patternName }}
    </h2>

    {{-- <div class="mb-10">
      {!!  $code !!}
    </div> --}}

    <div class="mb-10">
      @include('patterns.' . $key)
    </div>

    <pre><code class="html">{{ $code }}</code></pre>

  </div>

</x-layouts.app>
